using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Proyecto_Final
{
   public class DatosPrestamos
    {   private string Nombre;
        private float MontoPrestamo;
        private float TasaAnual;
        private float TasaMensual;
        private int Plazo;
        private float Cuota;

        private float interesPagados;

        public void solicitudDatosPrestamos()
        {
            Console.Write("Favor Ingrese su nombre:\n");
            Nombre = Console.ReadLine();
            Console.Write("Ingrese el monto del prestamo\n"+" RD$");
            MontoPrestamo = float.Parse(Console.ReadLine());
            /*Al momento de declarar la tasa utilizar , y no . para que asuma el formato correcto*/
            Console.Write("Ingrese la tasa anual del prestamo, Ej: 16,95\n");
            TasaAnual = float.Parse(Console.ReadLine());
            Console.Write("Ingrese el plazo del prestamo en meses\n");
            Plazo = int.Parse(Console.ReadLine());
        }
        public void CalcularPrestamo()
        {   
            Console.Write("\nPlacer saludarle Sr (a): "+Nombre+"\n");
            /*Calculo para el interes de manera mensual*/
            TasaMensual = (TasaAnual/100)/12;
            /*Calculo para la cuota mensual*/
            Cuota = TasaMensual+1;
            Cuota = (float)Math.Pow(Cuota, Plazo);
            Cuota = Cuota - 1;
            Cuota = TasaMensual / Cuota;
            Cuota = TasaMensual + Cuota;
            Cuota = MontoPrestamo * Cuota;
            Console.Write("La cuota a pagar por dicho prestamo es de RD$"+Cuota+" mensuales\n\n");
        }
        public void TablaAmortizacion()
        {
            int fila=1, i;
            float capitalPagado = 0;
            int contador = 0;
            Console.Write("Numero de Pago \t");
            Console.Write("\tFechas de pago\t\t");
            Console.Write("\t Cuota \t\t\t");
            Console.Write("Intereses \t\t");
            Console.Write("\tCapital Pagado \t\t");
            Console.Write("Monto del Prestamo");
            Console.WriteLine();
            Console.Write("\t0\t");
            Console.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{0}", "RD$"+MontoPrestamo);
            for(i = 1; i<= Plazo; i++)
               {   
                    Console.Write("\t{0}\t\t", fila);
                    /*Estructura DateTime que me permite obtener el dia de la consulta del prestamo*/
                    DateTime DT = DateTime.Now;
                    /*Impresion de la fecha en formato DD/MM/YY incrementando de uno en uno con el contador*/
                    Console.WriteLine(DT.AddMonths(contador).ToString("d"));
                    contador = contador+1;
                    Console.Write("\t\t\t\t\t\t\t{0}\t\t", "RD$"+Cuota);
                    interesPagados = TasaMensual * MontoPrestamo;
                    Console.Write("{0}\t\t", "RD$"+interesPagados);
                    capitalPagado = Cuota - interesPagados;
                    Console.Write("\t{0}\t\t", "RD$" +capitalPagado);
                    MontoPrestamo = MontoPrestamo - capitalPagado;
                    Console.WriteLine();
                    Console.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{0}", "RD$" +MontoPrestamo);
                    fila = fila+1;
                    Console.WriteLine();
            }
        }
        public void ConsultarPrestamo()
               {
                    string Consulta;
                    Console.Write("Desea consultar el valor a pagar por un prestamo? Ej: (si/no)\n");
                    Consulta = Console.ReadLine();
                    if (Consulta == "si")
                    {
                         DatosPrestamos DP = new DatosPrestamos();
                         DP.solicitudDatosPrestamos();
                         DP.CalcularPrestamo();
                         Console.Write("\t\t\t\t\t\t\t\t\tTabla de Amortización\n\n");
                         DP.TablaAmortizacion();
                    }
                    else
                    {
                         Console.WriteLine("Gracias por visitar nuestra calculadora de prestamos");
                         Console.ReadKey();
                    }
               }
           
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenidos a tu calculadora de prestamos\n\n");
            DatosPrestamos DPConsulta = new DatosPrestamos();
            DPConsulta.ConsultarPrestamo();
        }
    }
}
